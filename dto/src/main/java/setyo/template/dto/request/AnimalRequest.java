package setyo.template.dto.request;

import lombok.Data;
import setyo.template.dto.model.Animal;

@Data
public class AnimalRequest {
    private Animal animal;
}
