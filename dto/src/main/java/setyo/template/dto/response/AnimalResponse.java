package setyo.template.dto.response;

import lombok.Data;
import setyo.template.dto.model.Animal;

@Data
public class AnimalResponse extends GeneralResponse {
    private Animal animal;
}
