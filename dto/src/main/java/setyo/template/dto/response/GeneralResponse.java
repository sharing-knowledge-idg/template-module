package setyo.template.dto.response;

import lombok.Data;

@Data
public class GeneralResponse {
    private String rc;
    private String message;
}
