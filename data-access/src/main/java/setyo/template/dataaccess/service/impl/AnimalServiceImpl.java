package setyo.template.dataaccess.service.impl;

import org.springframework.stereotype.Service;
import setyo.template.dataaccess.model.Animal;
import setyo.template.dataaccess.repository.AnimalRepository;
import setyo.template.dataaccess.service.AnimalService;

@Service
public class AnimalServiceImpl implements AnimalService {

    private AnimalRepository animalRepository;

    public AnimalServiceImpl(AnimalRepository animalRepository) {
        this.animalRepository = animalRepository;
    }

    @Override
    public Animal getAnimalByName(String name) {
        return animalRepository.getAnimalByName(name);
    }

    @Override
    public Animal save(Animal animal) {
        return animalRepository.save(animal);
    }
}
