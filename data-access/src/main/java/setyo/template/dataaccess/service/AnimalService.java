package setyo.template.dataaccess.service;

import setyo.template.dataaccess.model.Animal;

public interface AnimalService {
    Animal getAnimalByName(String name);
    Animal save(Animal animal);
}
