package setyo.template.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import setyo.template.dataaccess.model.Animal;

@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long> {
    Animal getAnimalByName(String name);
}
