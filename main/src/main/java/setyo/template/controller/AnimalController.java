package setyo.template.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import setyo.template.dataaccess.model.Animal;
import setyo.template.dataaccess.service.AnimalService;
import setyo.template.dto.request.AnimalRequest;
import setyo.template.dto.response.AnimalResponse;

@Log4j
@RestController
public class AnimalController {

    private AnimalService animalService;
    private ObjectMapper objectMapper;

    public AnimalController(AnimalService animalService) {
        this.animalService = animalService;
        this.objectMapper = new ObjectMapper();
    }

    @PostMapping("/animal")
    public ResponseEntity<AnimalResponse> save(AnimalRequest request){
        AnimalResponse response = new AnimalResponse();
        try {
            Animal data = objectMapper.readValue(objectMapper.writeValueAsString(request.getAnimal()), Animal.class);
            data = animalService.save(data);

            response.setRc("00");
            response.setMessage("success");
            response.setAnimal(objectMapper.readValue(objectMapper.writeValueAsString(data),
                    setyo.template.dto.model.Animal.class));
        }catch (Exception e){

            response.setRc("99");
            response.setMessage("Failure : "+e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
        return ResponseEntity.ok(response);
    }
}
